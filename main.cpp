#include <iostream>
#include "executor/executor.h"

using std::chrono::operator""ms;

class Latch {
public:
    explicit Latch(size_t count) : counter_{count} {
    }

    void Wait() {
        std::unique_lock guard{mutex_};
        done_.wait(guard, [this] { return counter_ == 0; });
    }

    void Signal() {
        std::lock_guard guard{mutex_};
        if (--counter_ == 0) {
            done_.notify_all();
        }
    }

private:
    size_t counter_;
    std::mutex mutex_;
    std::condition_variable done_;
};

class LatchSignaler : public Task {
public:
    LatchSignaler(Latch* latch) : latch_{latch} {
    }

    void Run() override {
        latch_->Signal();
    }

private:
    Latch* latch_;
};

void test() {
    auto start = std::chrono::system_clock::now();

    static constexpr auto kCount = 1'000'000ul;

    auto pool = MakeThreadPoolExecutor(1);
    Latch latch{kCount};
    auto at = std::chrono::system_clock::now() + 5ms;

    for (auto i = 0ul; i < kCount; ++i) {
        auto task = std::make_shared<LatchSignaler>(&latch);
        task->SetTimeTrigger(at /*+ std::chrono::microseconds{(i * i) % 1000}*/);
        pool->Submit(task);
    }

    latch.Wait();

    auto end = std::chrono::system_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << std::endl;
}

int main() {
    test();
}