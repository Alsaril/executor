#include <executor/executor.h>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

using namespace std::chrono_literals;

struct EmptyTask : Task {
    void Run() override {
    }
};

TEST_CASE("Simple") {
    const auto num_threads = GENERATE(1, 2, 4);
    auto pool = MakeThreadPoolExecutor(num_threads);

    BENCHMARK("Simple:" + std::to_string(num_threads)) {
        auto task = std::make_shared<EmptyTask>();
        pool->Submit(task);
        task->Wait();
    };
}

TEST_CASE("FanoutFanin") {
    const auto num_threads = GENERATE(1, 2, 10);
    const auto size = GENERATE(1, 10, 100);
    auto pool = MakeThreadPoolExecutor(num_threads);

    BENCHMARK("FanoutFanin:" + std::to_string(num_threads) + ':' + std::to_string(size)) {
        auto first_task = std::make_shared<EmptyTask>();
        auto last_task = std::make_shared<EmptyTask>();

        for (auto i = 0; i < size; ++i) {
            auto middle_task = std::make_shared<EmptyTask>();
            middle_task->AddDependency(first_task);
            last_task->AddDependency(middle_task);
            pool->Submit(middle_task);
        }

        pool->Submit(first_task);
        pool->Submit(last_task);
        last_task->Wait();
    };
}

class Latch {
public:
    explicit Latch(size_t count) : counter_{count} {
    }

    void Wait() {
        std::unique_lock guard{mutex_};
        done_.wait(guard, [this] { return counter_ == 0; });
    }

    void Signal() {
        std::lock_guard guard{mutex_};
        if (--counter_ == 0) {
            done_.notify_all();
        }
    }

private:
    size_t counter_;
    std::mutex mutex_;
    std::condition_variable done_;
};

class LatchSignaler : public Task {
public:
    LatchSignaler(Latch* latch) : latch_{latch} {
    }

    void Run() override {
        latch_->Signal();
    }

private:
    Latch* latch_;
};

TEST_CASE("ScalableTimers") {
    static constexpr auto kCount = 100'000ul;
    const auto num_threads = GENERATE(1, 2, 5);
    auto pool = MakeThreadPoolExecutor(num_threads);

    BENCHMARK("ScalableTimers:" + std::to_string(num_threads)) {
        Latch latch{kCount};
        auto at = std::chrono::system_clock::now() + 5ms;

        for (auto i = 0ul; i < kCount; ++i) {
            auto task = std::make_shared<LatchSignaler>(&latch);
            task->SetTimeTrigger(at + std::chrono::microseconds{(i * i) % 1000});
            pool->Submit(task);
        }

        latch.Wait();
    };
}
