#include <executor/executor.h>

#include <thread>
#include <chrono>
#include <atomic>
#include <ranges>
#include <numeric>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>
#include <iostream>

using namespace std::chrono_literals;

struct FutureTest {
    std::shared_ptr<Executor> pool = MakeThreadPoolExecutor(2u);
};

TEST_CASE_METHOD(FutureTest, "InvokeVoid") {
    auto x = 0;
    auto future = pool->Invoke<Unit>([&] {
        x = 42;
        return Unit{};
    });

    future->Get();
    REQUIRE(x == 42);
}

TEST_CASE_METHOD(FutureTest, "InvokeString") {
    auto future = pool->Invoke<std::string>([] { return "Hello World"; });
    STATIC_REQUIRE(std::is_same_v<decltype(future->Get()), std::string>);

    REQUIRE(future->Get() == std::string{"Hello World"});
}

TEST_CASE_METHOD(FutureTest, "InvokeException") {
    static constexpr auto kMessage = "Test";
    auto future = pool->Invoke<Unit>([]() -> Unit { throw std::logic_error{kMessage}; });

    REQUIRE_THROWS_MATCHES(future->Get(), std::logic_error, Catch::Matchers::Message(kMessage));
}

TEST_CASE_METHOD(FutureTest, "Then") {
    auto future_a = pool->Invoke<std::string>([] { return "Foo Bar"; });

    auto future_b = pool->Then<Unit>(future_a, [future_a] {
        CHECK(future_a->IsFinished());
        CHECK(future_a->Get() == "Foo Bar");
        return Unit{};
    });

    future_b->Get();
    REQUIRE(future_b->IsFinished());
}

TEST_CASE_METHOD(FutureTest, "ThenIsNonBlocking") {
    auto start = std::chrono::steady_clock::now();

    auto future_a = pool->Invoke<std::string>([] {
        std::this_thread::sleep_for(100ms);
        return "Foo Bar";
    });

    auto future_b = pool->Then<Unit>(future_a, [future_a] { return Unit{}; });

    auto duration = std::chrono::steady_clock::now() - start;
    REQUIRE(duration < 50ms);
}

TEST_CASE_METHOD(FutureTest, "ThenChain") {
    static constexpr auto kN = 100;
    std::vector futures = {pool->Invoke<int>([] { return 1; })};
    for (auto i = 1; i < kN; ++i) {
        auto prev = futures.back();
        futures.push_back(pool->Then<int>(prev, [i, prev] {
            CHECK(prev->IsFinished());
            auto result = prev->Get();
            CHECK(result == i);
            return result + 1;
        }));
    }
    REQUIRE(futures.back()->Get() == kN);
}

TEST_CASE_METHOD(FutureTest, "WhenAll") {
    static constexpr auto kRange = std::views::iota(0ul, 100ul);
    auto start = std::chrono::steady_clock::now();
    std::atomic<size_t> count = 0;

    std::vector<FuturePtr<size_t>> all;
    for (auto i : kRange) {
        all.emplace_back(pool->Invoke<size_t>([&count, i] {
            std::this_thread::sleep_for(1ms);
            count++;
            return i;
        }));
    }

    auto results = pool->WhenAll(all)->Get();
    auto duration = std::chrono::steady_clock::now() - start;

    REQUIRE(count.load() == kRange.size());
    REQUIRE(std::ranges::equal(results, kRange));
    REQUIRE(duration < 100ms);
}

TEST_CASE_METHOD(FutureTest, "WhenAllWithThen") {
    auto future = pool->WhenAll(std::vector{pool->Invoke<int>([] { return 1; }),
                                            pool->Invoke<int>([] { return 2; }),
                                            pool->Invoke<int>([] { return 3; })});

    auto sum_future = pool->Then<int>(future, [future] {
        CHECK(future->IsFinished());
        auto results = future->Get();
        CHECK(results.size() == 3);
        auto sum = std::reduce(results.begin(), results.end());
        CHECK(sum == 6);
        return sum;
    });

    auto second_future = pool->WhenAll(std::vector{pool->Invoke<int>([] { return 4; }), sum_future,
                                                   pool->Invoke<int>([] { return 5; })});
    REQUIRE(second_future->Get() == std::vector{4, 6, 5});
}

TEST_CASE_METHOD(FutureTest, "WhenFirst") {
    auto start = std::chrono::steady_clock::now();

    auto first_future = pool->Invoke<int>([] {
        std::this_thread::sleep_for(100ms);
        return 1;
    });
    auto second_future = pool->Invoke<int>([] { return 2; });
    auto res_future = pool->WhenFirst(std::vector{first_future, second_future});

    auto result = res_future->Get();
    auto duration = std::chrono::steady_clock::now() - start;
    REQUIRE(result == 2);
    REQUIRE(duration < 50ms);
}

TEST_CASE_METHOD(FutureTest, "WhenFirstWithThen") {
    auto start = std::chrono::steady_clock::now();

    auto first_future = pool->Invoke<int>([] {
        std::this_thread::sleep_for(100ms);
        return 1;
    });
    auto second_future = pool->Invoke<int>([] {
        std::this_thread::sleep_for(50ms);
        return 2;
    });

    auto future = pool->WhenFirst(std::vector{first_future, second_future});
    auto res_future = pool->Then<int>(future, [future] {
        CHECK(future->IsFinished());
        return future->Get();
    });

    auto result = res_future->Get();
    auto duration = std::chrono::steady_clock::now() - start;
    CHECK(result == 2);
    CHECK(duration > 45ms);
    CHECK(duration < 75ms);
}

TEST_CASE_METHOD(FutureTest, "WhenAllBeforeDeadline") {
    static constexpr auto kN = 10;
    auto start = std::chrono::system_clock::now();

    std::vector<FuturePtr<int>> all;
    for (auto i = 0; i < kN; i++) {
        all.push_back(pool->Invoke<int>([&, i] { return i; }));
    }

    auto slow_future = pool->Invoke<Unit>([] {
        std::this_thread::sleep_for(100ms);
        return Unit{};
    });

    for (auto i = 0; i < kN; i++) {
        all.push_back(pool->Then<int>(slow_future, [i] { return kN + i; }));
    }

    auto result = pool->WhenAllBeforeDeadline(all, start + 50ms)->Get();
    auto duration = std::chrono::system_clock::now() - start;

    CHECK(result.size() == kN);
    CHECK(duration < 80ms);
}
