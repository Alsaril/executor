#pragma once

#include <atomic>
#include <functional>
#include <thread>

namespace tp {
template <class T>
class MPSCQueue {
    struct Node {
        T value;
        Node* next = nullptr;

        Node(T value) : value(value) {
        }
    };

    std::atomic<Node*> head_ = nullptr;

public:
    void Push(const T& value) {
        Node* next = new Node(value);
        Node* prev = head_.load(std::memory_order::relaxed);
        do {
            std::this_thread::yield();
            next->next = prev;
        } while (!head_.compare_exchange_weak(prev, next, std::memory_order::acq_rel));
    }

    void PopAll(std::function<void(const T&)> op) {
        Node* head = head_.load(std::memory_order::relaxed);
        while (!head_.compare_exchange_strong(head, nullptr, std::memory_order::acq_rel)) {
            std::this_thread::yield();
        }
        while (head) {
            auto next = head->next;
            op(head->value);
            delete head;
            head = next;
        }
    }

    ~MPSCQueue() {
        PopAll([]([[maybe_unused]] auto& el) {});
    }
};
}  // namespace tp
