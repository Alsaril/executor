#pragma once

#include <functional>

namespace tp {

class CancelableRunnable {
public:
    CancelableRunnable() {
    }

    CancelableRunnable(std::function<void()> body, std::function<void()> cancel)
        : body_(body), cancel_(cancel) {
    }

    void operator()() {
        body_();
    }

    void Cancel() {
        cancel_();
    }

private:
    std::function<void()> body_;
    std::function<void()> cancel_;
};

}  // namespace tp