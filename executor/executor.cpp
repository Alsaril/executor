#include "executor.h"

tp::FuturePtr<Unit> CompletionPolicy(tp::FuturePtr<Unit> future) {
    auto result_future = std::make_shared<tp::CompletableFuture<Unit>>();
    future->Then([result_future]([[maybe_unused]] tp::FutureResultPtr<Unit> result) {
        result_future->Completed(unit);
    });
    return result_future;
}

void Executor::Submit(std::shared_ptr<Task> task) {
    if (!running_) {
        task->future_->Canceled();
        return;
    }

    std::vector<tp::FuturePtr<Unit>> triggers;
    if (task->time_trigger_) {
        auto future = tp::Delayed(pool_, *task->time_trigger_, unit);
        triggers.push_back(future);
    }
    for (auto& trigger : task->triggers_) {
        triggers.push_back(CompletionPolicy(trigger->future_));
    }
    if (task->dependencies_.size() == 1) {
        triggers.push_back(CompletionPolicy(task->dependencies_.front()->future_));
    } else if (!task->dependencies_.empty()) {
        std::vector<tp::FuturePtr<Unit>> dependencies;
        for (auto& dep : task->dependencies_) {
            dependencies.push_back(CompletionPolicy(dep->future_));
        }
        tp::FuturePtr<std::vector<Unit>> f1 = tp::All(dependencies);
        tp::FuturePtr<Unit> future = tp::Map<std::vector<Unit>, Unit>(
            f1, []([[maybe_unused]] std::vector<Unit>& res) { return unit; });
        triggers.push_back(future);
    }
    if (triggers.empty()) {
        triggers.push_back(tp::Value(unit));
    }
    task->Free();
    auto future = triggers.size() == 1 ? triggers.front() : tp::Any(triggers);

    auto task_future = task->future_;
    std::function<Unit()> body([task]() {
        task->Run();
        return unit;
    });

    tp::Then<Unit, Unit>(future, task_future,
                         [body = std::move(body), &pool = this->pool_,
                          task_future]([[maybe_unused]] tp::FutureResultPtr<Unit> result) mutable {
                             SubmitBody<Unit>(pool, std::move(body), task_future);
                         });
}

std::shared_ptr<Executor> MakeThreadPoolExecutor(uint32_t num_threads) {
    return std::make_shared<Executor>(num_threads);
}