#pragma once

#include <functional>
#include <atomic>
#include <optional>
#include <memory>
#include <thread>

#include "mpsc_queue.h"
#include "utils.h"

namespace tp {

using std::chrono_literals::operator""ms;

enum class FutureOutcome { COMPLETED, FAILED, CANCELED };
enum class FutureState { PENDING, FINALIZING, COMPLETED, FAILED, CANCELED, MODIFYING };

template <class T>
struct FutureResult {
    FutureOutcome outcome;
    std::optional<T> result_;
    std::exception_ptr eptr_;
};

template <class T>
using FutureResultPtr = std::shared_ptr<FutureResult<T>>;

template <class T>
class CompletableFuture {
public:
    FutureState State() {
        auto state = state_.load(std::memory_order::acquire);
        if (state == FutureState::FINALIZING || state == FutureState::MODIFYING) {
            return FutureState::PENDING;
        }
        return state;
    }

    T& Result() {
        return *result_->result_;
    }

    std::exception_ptr Error() {
        return result_->eptr_;
    }

    void Then(std::function<void(FutureResultPtr<T>)> callback) {
        auto state = state_.load(std::memory_order::acquire);

        if (state == FutureState::PENDING) {
            if (state_.compare_exchange_strong(state, FutureState::MODIFYING,
                                               std::memory_order::acq_rel)) {
                callbacks_.Push(callback);
                state_.store(FutureState::PENDING);
                return;
            }
        }
        if (state == FutureState::FINALIZING) {
            while (state_ == FutureState::FINALIZING) {
                std::this_thread::yield();
            }
        }

        callback(result_);
    }

    void WithResult(FutureResultPtr<T> result) {
        auto state = FromOutcome(result->outcome);
        if (!Acquire()) {
            return;
        }

        result_ = result;

        Notify(state);
    }

    template <class Ts>
    void Completed(Ts&& value) {
        if (!Acquire()) {
            return;
        }

        auto result = std::make_shared<FutureResult<T>>();
        result->outcome = FutureOutcome::COMPLETED;
        result->result_ = std::forward<Ts>(value);
        result_ = result;

        Notify(FutureState::COMPLETED);
    }

    void Failed(std::exception_ptr eptr) {
        if (!Acquire()) {
            return;
        }

        auto result = std::make_shared<FutureResult<T>>();
        result->outcome = FutureOutcome::FAILED;
        result->eptr_ = eptr;
        result_ = result;

        Notify(FutureState::FAILED);
    }

    void Canceled() {
        if (!Acquire()) {
            return;
        }

        auto result = std::make_shared<FutureResult<T>>();
        result->outcome = FutureOutcome::CANCELED;
        result_ = result;

        Notify(FutureState::CANCELED);
    }

    void Wait() {
        while (true) {
            auto state = state_.load(std::memory_order::relaxed);
            if (state != FutureState::PENDING && state != FutureState::FINALIZING &&
                state != FutureState::MODIFYING) {
                return;
            }
            state_.wait(state);
        }
    }

private:
    std::atomic<FutureState> state_{FutureState::PENDING};
    FutureResultPtr<T> result_;
    MPSCQueue<std::function<void(FutureResultPtr<T>)>> callbacks_;

    FutureState FromOutcome(FutureOutcome outcome) {
        switch (outcome) {
            case FutureOutcome::COMPLETED:
                return FutureState::COMPLETED;
            case FutureOutcome::FAILED:
                return FutureState::FAILED;
            case FutureOutcome::CANCELED:
                return FutureState::CANCELED;
        }
        throw 1;
    }

    bool Acquire() {
        FutureState prev = FutureState::PENDING;
        while (!state_.compare_exchange_weak(prev, FutureState::FINALIZING,
                                             std::memory_order::acq_rel)) {
            if (prev != FutureState::PENDING &&
                prev != FutureState::MODIFYING) {  // allowed to proceed
                return false;
            }
            prev = FutureState::PENDING;
            std::this_thread::yield();
        }
        return true;
    }

    void Notify(FutureState state) {
        state_.store(state, std::memory_order::release);  // fence
        callbacks_.PopAll([this](auto& lambda) { lambda(result_); });
        state_.notify_all();
    }
};

template <class T>
using FuturePtr = std::shared_ptr<CompletableFuture<T>>;

}  // namespace tp