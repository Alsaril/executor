#pragma once

#include "future.h"
#include "thread_pool.h"
#include "utils.h"

namespace tp {

template <class T, class N>  // untie execution
void Then(FuturePtr<T> future, FuturePtr<N> result_future,
          std::function<void(FutureResultPtr<T>)> next) {
    future->Then([next, result_future](FutureResultPtr<T> result) {
        if (result->outcome == FutureOutcome::FAILED) {
            result_future->Failed(result->eptr_);
            return;
        }
        if (result->outcome == FutureOutcome::CANCELED) {
            result_future->Canceled();
            return;
        }
        if (result_future->State() == FutureState::CANCELED) {
            return;
        }

        next(result);

        //        CancelableRunnable cr([result, next]() { next(result); },
        //                              [result_future]() { result_future->Canceled(); });
        //
        //        pool->Submit(cr);
    });
}

template <class T>
FuturePtr<T> Delayed(ThreadPool& pool, std::chrono::system_clock::time_point at, T& value) {
    auto future = std::make_shared<tp::CompletableFuture<T>>();
    CancelableRunnable cr([future, value]() { future->Completed(value); },
                          [future]() { future->Canceled(); });
    pool.SubmitDelayed(at, cr);
    return future;
}

template <class T>
FuturePtr<T> Value(T& value) {
    auto future = std::make_shared<tp::CompletableFuture<T>>();
    future->Completed(value);
    return future;
}

template <class T>
FuturePtr<T> Canceled() {
    auto future = std::make_shared<tp::CompletableFuture<T>>();
    future->Canceled();
    return future;
}

template <class T>
class WaitingFuture : public CompletableFuture<std::vector<T>> {
    struct FatOptional {
        alignas(64) std::optional<T> value;
    };

public:
    WaitingFuture(const std::vector<FuturePtr<T>>& futures) : results_(futures.size()) {
    }

    void OnNext(size_t index, FutureResultPtr<T> result) {  // concurrent
        int val = 0;
        while (!state_.compare_exchange_weak(val, 1, std::memory_order::acq_rel)) {
            if (val == 2) {
                return;
            }
            std::this_thread::yield();
        }
        results_[index].value = *result->result_;
        state_.store(0, std::memory_order::release);
        if (completed_count_.fetch_add(1, std::memory_order::relaxed) + 1 == results_.size()) {
            Finish();
        }
    }

    void Finish() {
        int val = 0;
        while (!state_.compare_exchange_weak(val, 2, std::memory_order::acq_rel)) {
            if (val == 2) {
                return;
            }
            std::this_thread::yield();
        }
        std::vector<T> result;
        for (auto& res : results_) {
            if (res.value) {
                result.emplace_back(std::move(*res.value));
            }
        }
        this->Completed(std::move(result));
    }

private:
    std::atomic<size_t> completed_count_{0};
    std::atomic<int> state_{0};
    std::vector<FatOptional> results_;
};

template <class T>
FuturePtr<std::vector<T>> All(const std::vector<FuturePtr<T>>& futures) {
    auto result_future = std::make_shared<WaitingFuture<T>>(futures);
    for (size_t i = 0; i < futures.size(); ++i) {
        Then<T, std::vector<T>>(
            futures[i], result_future,
            [result_future, i](FutureResultPtr<T> result) { result_future->OnNext(i, result); });
    }
    return result_future;
}

template <class T>
FuturePtr<std::vector<T>> AllBefore(ThreadPool& pool, const std::vector<FuturePtr<T>>& futures,
                                    std::chrono::system_clock::time_point at) {
    auto result_future = std::make_shared<WaitingFuture<T>>(futures);
    for (size_t i = 0; i < futures.size(); ++i) {
        Then<T, std::vector<T>>(
            futures[i], result_future,
            [result_future, i](FutureResultPtr<T> result) { result_future->OnNext(i, result); });
    }
    auto terminate_future = Delayed(pool, at, unit);
    Then<Unit, std::vector<T>>(terminate_future, result_future,
                               [result_future]([[maybe_unused]] FutureResultPtr<Unit> result) {
                                   result_future->Finish();
                               });
    return result_future;
}

template <class T>
class ProxyFuture : public CompletableFuture<T> {
public:
    ProxyFuture(FuturePtr<T> next) : next_(next) {
    }

    void ResultAndReset(FutureResultPtr<T> result) {
        if (auto tmp = std::atomic_exchange(&next_, {}); tmp) {
            tmp->WithResult(result);
        }
    }

private:
    FuturePtr<T> next_;
};

template <class T>
FuturePtr<T> Any(const std::vector<FuturePtr<T>>& futures) {
    auto result_future = std::make_shared<CompletableFuture<T>>();
    // auto proxy_future = std::make_shared<ProxyFuture<T>>(result_future);
    for (auto& future : futures) {
        Then<T, T>(future, result_future, [result_future](FutureResultPtr<T> result) {
            result_future->WithResult(result);
        });
    }
    return result_future;
}

template <class T, class N>
FuturePtr<N> Map(FuturePtr<T> future, std::function<N(T&)> transform) {
    auto result_future = std::make_shared<CompletableFuture<N>>();
    Then<T, N>(future, result_future, [result_future, transform](FutureResultPtr<T> result) {
        result_future->Completed(transform(*result->result_));
    });
    return result_future;
}

}  // namespace tp