#pragma once

#include <memory>
#include <chrono>
#include <vector>
#include <functional>
#include <optional>

#include "thread_pool.h"
#include "future.h"
#include "future_combinators.h"
#include "utils.h"

template <class T>
class Future {
    friend class Executor;

public:
    Future() : future_(std::make_shared<tp::CompletableFuture<T>>()) {
    }

    Future(std::shared_ptr<tp::CompletableFuture<T>> future) : future_(future) {
    }

    virtual ~Future() = default;

    virtual void Run() {
    }

    T Get() {
        Wait();
        switch (future_->State()) {
            case tp::FutureState::COMPLETED:
                return future_->Result();
            case tp::FutureState::FAILED:
                std::rethrow_exception(future_->Error());
            default:
                throw std::exception();
        }
    }

    void AddDependency(std::shared_ptr<Future<T>> dep) {
        dependencies_.push_back(dep);
    }

    void AddTrigger(std::shared_ptr<Future<T>> dep) {
        triggers_.push_back(dep);
    }

    void SetTimeTrigger(std::chrono::system_clock::time_point at) {
        if (time_trigger_) {
            throw 1;
        }
        time_trigger_ = at;
    }

    // Task::Run() completed without throwing exception
    bool IsCompleted() {
        return future_->State() == tp::FutureState::COMPLETED;
    }

    // Task::Run() threw exception
    bool IsFailed() {
        return future_->State() == tp::FutureState::FAILED;
    }

    // Task was Canceled
    bool IsCanceled() {
        return future_->State() == tp::FutureState::CANCELED;
    }

    // Task either completed, failed or was Canceled
    bool IsFinished() {
        auto state = future_->State();
        return state == tp::FutureState::COMPLETED || state == tp::FutureState::FAILED ||
               state == tp::FutureState::CANCELED;
    }

    std::exception_ptr GetError() {
        return future_->Error();
    }

    void Cancel() {
        future_->Canceled();
    }

    void Wait() {
        future_->Wait();
    }

    void Free() {
        dependencies_.clear();
        triggers_.clear();
    }

private:
    std::vector<std::shared_ptr<Future<T>>> dependencies_;
    std::vector<std::shared_ptr<Future<T>>> triggers_;
    std::optional<std::chrono::system_clock::time_point> time_trigger_;

public:
    std::shared_ptr<tp::CompletableFuture<T>> future_;
};

using Task = Future<Unit>;

template <class T>
using FuturePtr = std::shared_ptr<Future<T>>;

template <class T>
FuturePtr<T> WrapFuture(tp::FuturePtr<T> future) {
    auto result = std::make_shared<Future<T>>(future);
    return result;
}

template <class T>
void SubmitBody(tp::ThreadPool& pool, std::function<T()> body, tp::FuturePtr<T> future);

class Executor {
public:
    Executor(size_t num_threads) : pool_(num_threads) {
        pool_.Start();
    }

    ~Executor() {
        WaitShutdown();
    }

    void Submit(std::shared_ptr<Task> task);

    void StartShutdown() {
        running_ = false;
        pool_.Stop();
    }

    void WaitShutdown() {
        StartShutdown();
        pool_.Wait();
    }

    template <class T>
    FuturePtr<T> Invoke(std::function<T()> fn) {
        auto future = std::make_shared<tp::CompletableFuture<T>>();
        SubmitBody<T>(pool_, std::move(fn), future);
        return WrapFuture(future);
    }

    template <class Y, class T>
    FuturePtr<Y> Then(FuturePtr<T> input, std::function<Y()> fn) {
        auto result_future = std::make_shared<tp::CompletableFuture<Y>>();
        tp::Then<T, Y>(input->future_, result_future,
                       [result_future, fn, input]([[maybe_unused]] tp::FutureResultPtr<T> result) {
                           result_future->Completed(fn());
                       });
        return WrapFuture(result_future);
    }

    template <class T>
    FuturePtr<std::vector<T>> WhenAll(std::vector<FuturePtr<T>> all) {
        std::vector<tp::FuturePtr<T>> futures;
        for (auto future : all) {
            futures.push_back(future->future_);
        }
        auto result_future = tp::All(futures);
        return WrapFuture(result_future);
    }

    template <class T>
    FuturePtr<T> WhenFirst(std::vector<FuturePtr<T>> all) {
        std::vector<tp::FuturePtr<T>> futures;
        for (auto future : all) {
            futures.push_back(future->future_);
        }
        auto result_future = tp::Any(futures);
        return WrapFuture(result_future);
    }

    template <class T>
    FuturePtr<std::vector<T>> WhenAllBeforeDeadline(
        std::vector<FuturePtr<T>> all, std::chrono::system_clock::time_point deadline) {
        std::vector<tp::FuturePtr<T>> futures;
        for (auto future : all) {
            futures.push_back(future->future_);
        }
        auto result_future = tp::AllBefore(pool_, futures, deadline);
        return WrapFuture(result_future);
    }

private:
    tp::ThreadPool pool_;
    std::atomic<bool> running_{true};
};

template <class T>
void SubmitBody(tp::ThreadPool& pool, std::function<T()> body, tp::FuturePtr<T> future) {
    tp::CancelableRunnable cr(
        [body = std::move(body), future]() mutable {
            if (future->State() == tp::FutureState::CANCELED || !body) {
                future->Canceled();
                return;
            }

            try {
                auto outcome = body();
                body = nullptr;
                future->Completed(outcome);
            } catch (...) {
                body = nullptr;
                future->Failed(std::current_exception());
            }
        },
        [future]() { future->Canceled(); });
    pool.Submit(cr);
}

std::shared_ptr<Executor> MakeThreadPoolExecutor(uint32_t num_threads);
