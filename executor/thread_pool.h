#pragma once

#include <functional>
#include <vector>
#include <memory>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <deque>
#include <queue>

#include "runnable.h"

namespace tp {

class ThreadPool {
public:
    ThreadPool(size_t workers_size) : workers_size_(workers_size), timer_queue_(custom_less_) {
    }
    ThreadPool(const ThreadPool&) = delete;
    ThreadPool(ThreadPool&&) = delete;

    void Start() {
        for (size_t i = 0; i < workers_size_; ++i) {
            workers_.emplace_back([this]() { WorkerLoop(); });
        }
    }

    void Stop() {
        running_ = false;
        std::unique_lock lock(mutex_);
        cv_.notify_all();
    }

    void Wait() {
        std::unique_lock lock(mutex_);
        if (terminated_) {
            return;
        }
        terminated_ = true;
        running_ = false;
        cv_.notify_all();
        lock.unlock();

        for (auto& worker : workers_) {
            worker.join();
        }

        lock.lock();
        workers_.clear();

        while (!queue_.empty()) {
            queue_.front().Cancel();
            queue_.pop_front();
        }

        while (!timer_queue_.empty()) {
            timer_queue_.Top().second.Cancel();
            timer_queue_.pop();
        }
    }

    void Submit(CancelableRunnable callable) {
        std::unique_lock lock(mutex_);
        if (!running_) {
            lock.unlock();
            callable.Cancel();
            return;
        }
        queue_.push_back(callable);
        cv_.notify_one();
    }

    void SubmitDelayed(std::chrono::system_clock::time_point at, CancelableRunnable callable) {
        std::unique_lock lock(mutex_);
        if (!running_) {
            lock.unlock();
            callable.Cancel();
            return;
        }
        timer_queue_.emplace(at, callable);
        cv_.notify_one();
    }

private:
    size_t workers_size_;
    std::vector<std::thread> workers_;
    std::mutex mutex_;
    std::condition_variable cv_;
    std::atomic<bool> running_{1};
    bool terminated_ = false;

    std::deque<CancelableRunnable> queue_;
    using Type = std::pair<std::chrono::system_clock::time_point, CancelableRunnable>;

    struct {
        bool operator()(const Type& l, const Type& r) const {
            return l.first >= r.first;
        }
    } custom_less_;

    template <class T, class C>
    class PriorityQueue : public std::priority_queue<T, std::vector<T>, C> {
    public:
        PriorityQueue(C& comp) : std::priority_queue<T, std::vector<T>, C>(comp) {
        }

        T& Top() {
            return std::priority_queue<T, std::vector<T>, C>::c.front();
        }
    };

    PriorityQueue<Type, decltype(custom_less_)> timer_queue_;

    void WorkerLoop() {
        while (running_) {
            std::unique_lock lock(mutex_);
            if (!running_ || terminated_) {
                return;
            }

            if (!queue_.empty()) {
                auto callable = queue_.front();
                queue_.pop_front();
                lock.unlock();
                callable();
                continue;
            }

            if (!timer_queue_.empty()) {
                auto now = std::chrono::system_clock::now();
                auto next_time = timer_queue_.top().first;

                if (now > next_time) {
                    auto callable = timer_queue_.Top().second;
                    timer_queue_.pop();
                    lock.unlock();
                    callable();
                    continue;
                }

                cv_.wait_until(lock, next_time);
                continue;
            }

            cv_.wait(lock);
        }
    }
};

}  // namespace tp