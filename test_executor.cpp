#include <executor/executor.h>

#include <thread>
#include <chrono>
#include <atomic>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace std::chrono_literals;

template <uint32_t N>
struct ExecutorTest {
    std::shared_ptr<Executor> pool = MakeThreadPoolExecutor(N);
};

#define TEST(name) \
    TEMPLATE_TEST_CASE_METHOD_SIG(ExecutorTest, name, "", ((uint32_t N), N), 1, 2, 10)

TEST("Destructor") {
}

TEST("StartShutdown") {
    auto& pool = ExecutorTest<N>::pool;
    pool->StartShutdown();
}

TEST("StartTwiceAndWait") {
    auto& pool = ExecutorTest<N>::pool;
    pool->StartShutdown();
    pool->StartShutdown();
    pool->WaitShutdown();
}

struct TestTask : Task {
    void Run() override {
        completed = true;
        ++counter;
    }

    ~TestTask() {
        INFO("Seems like task body was run multiple times");
        CHECK(counter < 2);
    }

    bool completed = false;
    std::atomic<int> counter = 0;
};

struct SlowTask : Task {
    void Run() override {
        std::this_thread::sleep_for(1ms);
        completed = true;
    }

    bool completed = false;
};

struct FailedTask : Task {
    void Run() override {
        throw std::logic_error{"Failed"};
    }
};

TEST("RunSingleTask") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();

    pool->Submit(task);
    task->Wait();

    CHECK(task->completed);
    CHECK(task->IsFinished());
    CHECK(task->IsCompleted());
    CHECK_FALSE(task->IsCanceled());
    CHECK_FALSE(task->IsFailed());
}

TEST("RunSingleFailingTask") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<FailedTask>();

    pool->Submit(task);
    task->Wait();

    CHECK_FALSE(task->IsCompleted());
    CHECK_FALSE(task->IsCanceled());
    CHECK(task->IsFailed());
    CHECK_THROWS_AS(std::rethrow_exception(task->GetError()), std::logic_error);
}

TEST("CancelSingleTask") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    task->Cancel();
    task->Wait();

    CHECK_FALSE(task->IsCompleted());
    CHECK(task->IsCanceled());
    CHECK_FALSE(task->IsFailed());

    pool->Submit(task);
    task->Wait();

    CHECK_FALSE(task->IsCompleted());
    CHECK(task->IsCanceled());
    CHECK_FALSE(task->IsFailed());
}

TEST("TaskWithSingleDependency") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto dependency = std::make_shared<TestTask>();

    task->AddDependency(dependency);
    pool->Submit(task);

    std::this_thread::sleep_for(3ms);
    CHECK_FALSE(task->IsFinished());

    pool->Submit(dependency);
    task->Wait();
    CHECK(task->IsCompleted());
    CHECK(dependency->IsCompleted());
}

TEST("TaskWithSingleCompletedDependency") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto dependency = std::make_shared<TestTask>();

    task->AddDependency(dependency);
    pool->Submit(dependency);
    dependency->Wait();

    std::this_thread::sleep_for(3ms);
    REQUIRE_FALSE(task->IsFinished());

    pool->Submit(task);
    task->Wait();
    CHECK(task->IsCompleted());
}

TEST("FailedDependencyIsConsideredCompleted") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto dependency = std::make_shared<FailedTask>();

    task->AddDependency(dependency);
    pool->Submit(task);

    std::this_thread::sleep_for(3ms);
    CHECK_FALSE(task->IsFinished());

    pool->Submit(dependency);
    task->Wait();
    CHECK(task->IsCompleted());
    CHECK(dependency->IsFailed());
}

TEST("CanceledDependencyIsConsideredCompleted") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto dependency = std::make_shared<TestTask>();

    task->AddDependency(dependency);
    pool->Submit(task);

    std::this_thread::sleep_for(3ms);
    CHECK_FALSE(task->IsFinished());

    dependency->Cancel();
    task->Wait();
    CHECK(task->IsCompleted());
    CHECK(dependency->IsCanceled());
}

struct RecursiveTask : Task {
    RecursiveTask(uint32_t n, std::shared_ptr<Executor> executor)
        : n_{n}, executor_{std::move(executor)} {
    }

    void Run() override {
        if (n_) {
            executor_->Submit(std::make_shared<RecursiveTask>(n_ - 1, executor_));
        } else {
            executor_->StartShutdown();
        }
    }

private:
    const uint32_t n_;
    const std::shared_ptr<Executor> executor_;
};

TEST("RunRecursiveTask") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<RecursiveTask>(100u, pool);
    pool->Submit(task);
    pool->WaitShutdown();
}

TEST("TaskWithSingleTrigger") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto trigger = std::make_shared<TestTask>();

    task->AddTrigger(trigger);
    pool->Submit(task);

    std::this_thread::sleep_for(3ms);
    CHECK_FALSE(task->IsFinished());

    pool->Submit(trigger);
    task->Wait();
    CHECK(task->IsCompleted());
    CHECK(trigger->IsCompleted());
}

TEST("TaskWithSingleCompletedTrigger") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto trigger = std::make_shared<TestTask>();

    task->AddTrigger(trigger);
    pool->Submit(trigger);
    trigger->Wait();
    CHECK_FALSE(task->IsFinished());

    pool->Submit(task);
    task->Wait();
    CHECK(task->IsCompleted());
}

TEST("TaskWithTwoTrigger") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto trigger_a = std::make_shared<TestTask>();
    auto trigger_b = std::make_shared<TestTask>();

    task->AddTrigger(trigger_a);
    task->AddTrigger(trigger_b);

    pool->Submit(task);
    pool->Submit(trigger_b);
    pool->Submit(trigger_a);

    task->Wait();
    CHECK(task->IsCompleted());
    CHECK(task->completed);
    CHECK((trigger_a->IsCompleted() || trigger_b->IsCompleted()));
}

TEST("TriggerAndDependencies") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto dep1 = std::make_shared<TestTask>();
    auto dep2 = std::make_shared<TestTask>();
    auto trigger = std::make_shared<TestTask>();

    task->AddDependency(dep1);
    task->AddDependency(dep2);
    task->AddTrigger(trigger);
    dep2->AddTrigger(trigger);

    pool->Submit(dep1);
    pool->Submit(dep2);
    pool->Submit(task);
    std::this_thread::sleep_for(20ms);
    dep1->Wait();
    CHECK(dep1->IsCompleted());
    CHECK_FALSE(dep2->IsFinished());
    CHECK_FALSE(task->IsFinished());

    pool->Submit(trigger);
    task->Wait();
    CHECK(task->IsCompleted());
    dep2->Wait();
    CHECK(dep2->IsCompleted());
}

TEST("MultipleDependencies") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto dep1 = std::make_shared<TestTask>();
    auto dep2 = std::make_shared<TestTask>();

    task->AddDependency(dep1);
    task->AddDependency(dep2);
    pool->Submit(task);

    std::this_thread::sleep_for(3ms);
    CHECK_FALSE(task->IsFinished());

    pool->Submit(dep1);
    dep1->Wait();

    std::this_thread::sleep_for(3ms);
    CHECK_FALSE(task->IsFinished());

    pool->Submit(dep2);
    task->Wait();
    CHECK(task->IsCompleted());
}

static auto Now() {
    return std::chrono::system_clock::now();
}

TEST("TaskWithSingleTimeTrigger") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto start = Now();

    task->SetTimeTrigger(start + 40ms);
    pool->Submit(task);

    std::this_thread::sleep_for(20ms);
    CHECK_FALSE(task->IsFinished());

    task->Wait();
    auto duration = Now() - start;
    CHECK(task->IsCompleted());
    CHECK(duration > 30ms);
    CHECK(duration < 60ms);
}

TEST("TaskTriggeredByTimeAndDep") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<TestTask>();
    auto dep = std::make_shared<TestTask>();

    task->AddDependency(dep);
    task->SetTimeTrigger(Now() + 40ms);

    pool->Submit(task);
    pool->Submit(dep);

    std::this_thread::sleep_for(20ms);
    CHECK(task->IsCompleted());
}

TEST("MultipleTimerTriggers") {
    auto& pool = ExecutorTest<N>::pool;
    auto task_a = std::make_shared<TestTask>();
    auto task_b = std::make_shared<TestTask>();
    auto start = Now();

    task_a->SetTimeTrigger(start + 40ms);
    task_b->SetTimeTrigger(start + 5ms);

    pool->Submit(task_a);
    pool->Submit(task_b);

    std::this_thread::sleep_for(20ms);
    CHECK_FALSE(task_a->IsFinished());
    CHECK(task_b->IsCompleted());

    task_a->Wait();
    auto duration = Now() - start;
    CHECK(task_a->IsCompleted());
    CHECK(duration < 60ms);
}

TEST("MultipleTimerTriggersWithReverseOrder") {
    auto& pool = ExecutorTest<N>::pool;
    auto task_a = std::make_shared<TestTask>();
    auto task_b = std::make_shared<TestTask>();

    task_a->SetTimeTrigger(Now() + 50ms);
    task_b->SetTimeTrigger(Now() + 3ms);

    pool->Submit(task_b);
    pool->Submit(task_a);

    task_b->Wait();
    CHECK_FALSE(task_a->IsFinished());
}

TEST("PossibleToCancelAfterSubmit") {
    auto& pool = ExecutorTest<N>::pool;
    std::vector<std::shared_ptr<SlowTask>> tasks;

    for (auto i = 0; i < 1'000; ++i) {
        auto& task = tasks.emplace_back(std::make_shared<SlowTask>());
        pool->Submit(task);
        task->Cancel();
    }
    pool.reset();

    for (const auto& t : tasks) {
        if (!t->completed) {
            return;
        }
    }
    FAIL("Seems like Cancel() doesn't affect Submitted tasks");
}

TEST("CancelAfterDestroyOfExecutor") {
    auto& pool = ExecutorTest<N>::pool;
    auto first = std::make_shared<TestTask>();
    auto second = std::make_shared<TestTask>();

    first->AddDependency(second);
    pool->Submit(first);
    pool.reset();

    first->Cancel();
    second->Cancel();
}

struct RecursiveGrowingTask : Task {
    RecursiveGrowingTask(uint32_t n, uint32_t fanout, std::shared_ptr<Executor> executor)
        : n_{n}, fanout_{fanout}, executor_{std::move(executor)} {
    }

    void Run() override {
        if (n_) {
            for (auto i = 0u; i < fanout_; ++i) {
                auto task = std::make_shared<RecursiveGrowingTask>(n_ - 1, fanout_, executor_);
                executor_->Submit(std::move(task));
            }
        } else {
            executor_->StartShutdown();
        }
    }

private:
    const uint32_t n_;
    const uint32_t fanout_;
    const std::shared_ptr<Executor> executor_;
};

TEST("NoDeadlockWhenSubmittingFromTaskBody") {
    auto& pool = ExecutorTest<N>::pool;
    auto task = std::make_shared<RecursiveGrowingTask>(5, 10, pool);
    pool->Submit(task);
    pool->WaitShutdown();
}
